# my.awesomeBible United

Dieses Repository existiert, um Informationen über my.awesomeBible zu geben, und Issues die weder Front- oder Backend betreffen zu sammeln.

Benutze bitte für Feature-Vorschläge oder um Fragen zu stellen den [Issues-Tab](https://codeberg.org/awesomebible/my-united/issues).

### [Frontend-Repository](https://codeberg.org/awesomebible/my-frontend)
Das my.awesomeBible Frontend - also der Teil den der Nutzer sieht - ist mit [Vue](https://vuejs.org) geschrieben und nutzt [TailwindCSS](https://tailwindcss.com) fürs Styling.

### [Backend](https://supabase.io)
Als Backend benutzt my.awesomeBible [Supabase](https://supabase.io).

Supabase ist ein Open Source Backend-as-A-Service.
Die Daten die an Supabase weitergegeben werden, unterliegen dessen [Datenschutzerklärung](https://supabase.io/docs/company/privacy), außerdem werden sie ausschließlich in deutschen Rechenzentren gespeichert.


### [Bibel API](https://codeberg.org/awesomebible/bible-api)
In diesem Repository befindet sich die Bibel-API die für my.awesomeBbible verwendet wird.

## Unsere Mission ⭐
awesomeBible hat sich als Ziel gesetzt, *jedem* von Gottes Genialem Plan zu erzählen.

my.awesomeBible baut darauf auf.
Mit diesem Projekt wollen wir Christen helfen, Gottes Wort dort zu hören, wo sie sind - mit ihm interagieren zu können, und es teilen zu können.

Wir bauen my.awesomeBible nicht für Profit, sondern weil es etwas ist, was wir uns selbst wünschen.
Wir haben my.awesomeBible mit den Werten gebaut, die uns wichtig sind - Gemeinschaft, Freiheit und Datenschutz - und nicht um das nächste Billion-Dollar-Unicorn zu werden.

## Mitmachen 🦄
## Vorschläge ✍
Wir brauchen deine Hilfe!
Für ein Projekt, dass sich auf die Fahne geschreiben hat, User-Focused zu sein, brauchen wir Feedback. Eine Menge Feedback.

Was brauchst du, um dich auf Gottes Wort zu konzentrieren?
Was ist dir besonders wichtig im Austausch mit anderen?
Was ist deine Lieblingsschriftart?

Egal was es ist: Keine Idee ist zu dumm, kein Wunsch zu wenig.

Schreib uns [per Email](mailto:hi@awesomebible.de), [über dieses Kontaktformular](https://awesomebible.de/kontakt/) oder öffne ein [Codeberg Issue](https://codeberg.org/awesomebible/my-united/issues/new).

## Entwicklung 🛠
Wenn du dich mit PHP, Vue, TailwindCSS oder MariaDB auskennst und Lust hast, an der Entwicklung von my.awesomeBible zu helfen, dann schau doch hier vorbei:

### [Frontend](https://codeberg.org/awesomebible/my-frontend) (Vue, TailwindCSS)

## Design 🍥
Programmieren ist nicht dein Ding? Du designst, zeichnest oder entwirfst lieber?
Auch dann kannst du uns helfen!
Oft wird für ein Feature ein Mockup oder ein Design benötigt, alle Issues wo ein Design benötigt wird, sind [hier](https://codeberg.org/awesomebible/my-frontend/issues?q=&type=all&state=open&labels=16018&milestone=0&assignee=0) und [hier](https://codeberg.org/awesomebible/my-united/issues?q=&type=all&sort=&state=open&labels=16008&milestone=0&assignee=0) zu finden.
